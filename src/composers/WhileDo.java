package composers;

import state_machine.ComposableState;
import state_machine.IBool;
import state_machine.IState;
import state_machine.StateSignal;
import state_machine.StateStatus;

public class WhileDo<Context> extends ComposableState<Context>{
	
	IState<Context> state;
	IBool<Context> predicate;
	public WhileDo(IBool<Context> predicate, IState<Context> state) {
		if(state == null) {
			throw new IllegalArgumentException();
		}
		
		if(predicate == null) {
			throw new IllegalArgumentException();
		}
		
		this.predicate = predicate;
		this.state = state;
	}
	
	@Override
	public void EnterState(Context context) {
		state.EnterState(context);
	}

	@Override
	public StateSignal<Context> Update(Context context) {
		if(predicate.Continue(context) == false) {
			return new StateSignal<Context>(null, StateStatus.End);
		}
		return state.Update(context);
	}

	@Override
	public void ExitState(Context context) {
		state.ExitState(context);
	}

}
