package composers;

import state_machine.ComposableState;
import state_machine.IState;
import state_machine.StateSignal;
import state_machine.StateStatus;

public class Then<Context> extends ComposableState<Context> {

	boolean use_next;
	boolean do_next_init;
	
	IState<Context> first_state;
	IState<Context> next_state;
	
	public Then(IState<Context> firstState, IState<Context> then) {
		if(firstState == null) {
			throw new IllegalArgumentException();
		}
		if(then == null) {
			throw new IllegalArgumentException();
		}
		
		first_state = firstState;
		next_state = then;
		use_next = false;
		do_next_init = true;
	}
	
	@Override
	public void EnterState(Context context) {
		first_state.EnterState(context);
	}

	@Override
	public StateSignal<Context> Update(Context context) {
		if(use_next == false) {	//SOB return_next = false fdjsaklfjdsaklfjdsalkfjdsalk NO LINTS??????????????
			StateSignal<Context> first_return = first_state.Update(context);
			if(first_return.Status == StateStatus.End) {
				use_next = true;
			} else {
				return first_return;
			}
		}
		
		if(use_next == true) {
			if(do_next_init) {
				first_state.ExitState(context);
				next_state.EnterState(context);
				do_next_init = false;
			}
			
			StateSignal<Context> next_return = next_state.Update(context);
			return next_return;
		}
		
		return new StateSignal<Context>(null, StateStatus.Continue);
	}

	@Override
	public void ExitState(Context context) {
		//Is this needed??
		if(use_next == false && do_next_init == true) {
			first_state.ExitState(context);
		} else {
			next_state.ExitState(context);
		}
	}
}
