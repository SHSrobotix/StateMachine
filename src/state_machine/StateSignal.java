package state_machine;

public class StateSignal<Context> {
	public IState<Context> Next;
	public StateStatus Status;
	
	///End
	public StateSignal() {
		Next = null;
		Status = StateStatus.End;
	}
	
	///Continue
	public StateSignal(IState<Context> next) {
		Next = next;
		Status = StateStatus.Continue;
	}
	
	public StateSignal(IState<Context> next, StateStatus status) {
		Next = next;
		Status = status;
	}
}
