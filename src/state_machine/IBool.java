package state_machine;

public interface IBool<Context> {
	public boolean Continue(Context context);
}
