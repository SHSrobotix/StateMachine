package state_machine;

import composers.Then;
import composers.WhileDo;

public abstract class ComposableState<Context> implements IState<Context> {
	public Then<Context> then(IState<Context> next) {
		return new Then<Context>(this, next);
	}
	
	public WhileDo<Context> while_do(IBool<Context> predicate, IState<Context> state) {
		return new WhileDo<Context>(predicate, state);
	}

	@Override
	public abstract void EnterState(Context context);

	@Override
	public abstract StateSignal<Context> Update(Context context);

	@Override
	public abstract void ExitState(Context context);
}
